#ifndef GRAPH_H
#define GRAPH_H

#include <map>
#include <vector>
#include <set>

#include "types/ofTypes.h"

enum NodeType {QUERY=0, PAPER, AUTHOR, KEYWORD, UNKNOWN};

class Node
{
public:
  Node();

  NodeType getType() const;
  void setType(NodeType type);
  uint64_t getId() const;

  const std::string & getStringId() const;
  void setStringId(const std::string & id);

  const std::string & displayName() const;
  void setDisplayName(const std::string & displayName);
 
private:
  NodeType m_type;
  uint64_t m_id;
  std::string m_displayName;
  std::string m_stringId;

  friend class Graph;
};

class Graph
{
    public:
        Graph();
        virtual ~Graph();

        void addNode(const std::shared_ptr<Node> & node);
        void addLink(uint64_t id1, uint64_t id2);
        bool linked(uint64_t id1, uint64_t id2) const;

        const std::vector<std::shared_ptr<Node> > & getNodes() const;

    private:
      std::vector<std::shared_ptr<Node> > m_nodes;
      std::map<uint64_t, std::shared_ptr<Node> > m_nodeMap;
      std::map<uint64_t, std::vector<uint64_t> > m_links;
      std::set<std::pair<uint64_t, uint64_t> > m_linkSet;
};

#endif // GRAPH_H
