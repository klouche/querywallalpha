#include "ofApp.h"

#include "CursorFeedback.hpp"
#include "FigureFeedback.hpp"
#include "TapFeedback.hpp"
#include "HandFeedback.hpp"

#include "EventClient.hpp"
#include "InputGestureDirectFingers.hpp"

#include <iostream>

namespace {
    class Test : public EventClient
    {
    public:
        Test()
        {
            registerEvent(InputGestureDirectFingers::Instance().newCursor, &Test::enter);
        }

        void enter(InputGestureDirectFingers::newCursorArgs & args) {
            std::cout << "new cursor" << std::endl;
        }
    };
}
//--------------------------------------------------------------
void ofApp::setup(){
    tableApp.setup();

    new CursorFeedback();
    new FigureFeedback();
    new TapFeedback();
	
	//ofTrueTypeFont::setGlobalDpi(72);

    dock.setup("Machine Learning");

    new Test();
}

//--------------------------------------------------------------
void ofApp::update(){
    dock.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    dock.draw();
    tableApp.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
