//
//  UINode.h
//  emptyExample
//
//  Created by Khalil Klouche on 09/04/14.
//
//

#ifndef __emptyExample__UINode__
#define __emptyExample__UINode__

#include <iostream>
#include "ofMain.h"

#include <boost/variant.hpp>

#include "Graph.h"

class Cluster;

// Not sure if this is a nice way to emulate algebraic data types.
// At least we can use non-POD types unlike with tagged unions
enum NodeStatusType
{
  NODE_IN_NOWHERE = 1,
  NODE_IN_DOCK,
  NODE_IN_CLUSTER,
  NODE_IN_FREE_AREA
};

template<size_t>
struct NodeStatusInfo;

template<>
struct NodeStatusInfo<NODE_IN_NOWHERE>
{
};

template<>
struct NodeStatusInfo<NODE_IN_DOCK>
{
  Cluster * cluster;
};

template<>
struct NodeStatusInfo<NODE_IN_CLUSTER>
{
  Cluster * cluster;
};

template<>
struct NodeStatusInfo<NODE_IN_FREE_AREA>
{
};

typedef boost::variant<
		NodeStatusInfo<NODE_IN_NOWHERE>,
		NodeStatusInfo<NODE_IN_DOCK>,
		NodeStatusInfo<NODE_IN_CLUSTER>,
		NodeStatusInfo<NODE_IN_FREE_AREA>
	> NodeStatusVariant;


/// TODO: make this a proper class with some helpers
typedef std::pair<NodeStatusType, NodeStatusVariant> NodeStatus;

class UINode{
public:
	UINode();

	void setup(const std::shared_ptr<Node> & node, const std::string & name, ofVec2f position);
	void update();
	void draw();
	
	void move(float xSpeed, float ySpeed);
	bool isWithin(float x, float y);
	float getDist(float x, float y);
	ofVec2f getPosition() const;
	ofVec2f getDisplayPosition() const;
	void setPosition(ofVec2f pos);
	void setDisplayPosition(ofVec2f pos);

	//while grabbed
	void setGrabbed(bool grabbed);
	bool getFromDock();

	// Status currently in effect
	NodeStatus getStatus() const;
	// Status where we might be in the future (eg. if we stop dragging here)
	NodeStatus getPredictedStatus() const;

	void setPredictedStatus(const NodeStatus & status);
	void setStatus(const NodeStatus & status);
	
	std::shared_ptr<Node> getNode();

	bool docked() const;
private:
	NodeStatus m_status;
	NodeStatus m_predictedStatus;

	std::shared_ptr<Node> m_node;
	std::string m_name;
	ofVec2f m_position; // actual position
	ofVec2f m_displayPosition; // displayed position (for moving attempts)
	float m_r, m_rTarget;
	bool m_grabbed;
	ofTrueTypeFont font;
};

#endif /* defined(__emptyExample__UINode__) */
