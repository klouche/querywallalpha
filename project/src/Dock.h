//
//  Dock.h
//  emptyExample
//
//  Created by Khalil Klouche on 09/04/14.
//
//

#ifndef __emptyExample__Dock__
#define __emptyExample__Dock__

#include <iostream>
#include <vector>

#include "ofMain.h"
#include "Cluster.h"
#include "ResultFetcher.h"

#include "EventClient.hpp"
#include "InputGestureDirectFingers.hpp"


struct NodeStatusChangeArgs
{
  std::shared_ptr<UINode> node;
};

class Dock : public EventClient {

public:
    void setup(string query0);
    void update();
    void draw();

    ofEvent<NodeStatusChangeArgs> nodeChanged;

    void newCursor(InputGestureDirectFingers::newCursorArgs & args);
    void updateCursor(InputGestureDirectFingers::updateCursorArgs & args);
    void removeCursor(InputGestureDirectFingers::removeCursorArgs & args);

    bool isInsideDock(ofVec2f pos) const;

    // Get the cluster that contains given x coordinate, if any
    std::shared_ptr<Cluster> getCluster(float x);

    std::shared_ptr<UINode> getNodeAt(ofVec2f location, std::shared_ptr<Cluster> * insideCluster = 0);
private:
    void nodeChangedCallback(NodeStatusChangeArgs & args);

    typedef std::vector< std::pair< QueryParameters, std::shared_ptr<UINode> > > QueryQueue;
    QueryQueue m_queryQueue;

    typedef std::shared_ptr<Poco::ActiveResult<std::shared_ptr<Result> > > ActiveResultPtr;
    std::pair<std::shared_ptr<UINode>, ActiveResultPtr > m_newResult;

    typedef std::map<int, ofVec2f> CursorMap;
	CursorMap m_cursors;
    typedef std::map<int, NodeStatusChangeArgs> GrabMap;
    GrabMap m_grabbedCursors;
    float m_width;
    float m_xOffset;            //horizontal scroll position
    float m_handlePos;    //separator vertical position
    string m_query;

    typedef std::vector<std::shared_ptr<Cluster> > Clusters;
    Clusters m_clusters;
    ResultFetcher m_fetcher;

};

#endif /* defined(__emptyExample__Dock__) */
