//
//  UINode.cpp
//  emptyExample
//
//  Created by Khalil Klouche on 09/04/14.
//
//

#include "UINode.h"

const float s_r = 8;
const float s_rGrabbed = 20;
const float s_rVariation = 0.3;
const float s_rTouch = 20;


UINode::UINode()
	: m_grabbed(false)
{
}

void UINode::setup(const std::shared_ptr<Node> & node, const std::string & name, ofVec2f position){
	m_node = node;
	m_name = name;
	m_position = position;
	m_displayPosition = position;
	m_rTarget = s_r;
	m_r = 0;
	font.loadFont("Roboto-Condensed.ttf", 11);
	font.setLetterSpacing(1.1);
}

ofVec2f UINode::getPosition() const
{
	return m_position;
}

ofVec2f UINode::getDisplayPosition() const
{
	return m_displayPosition;
}

void UINode::setPosition(ofVec2f pos)
{
    m_position = pos;
}

void UINode::setDisplayPosition(ofVec2f pos)
{
    m_displayPosition = pos;
}

void UINode::update(){
	m_rTarget = m_grabbed? s_rGrabbed: s_r;
	m_r += s_rVariation * (m_rTarget - m_r);
	if (!m_grabbed) m_displayPosition += s_rVariation * (m_position-m_displayPosition);
}

void UINode::draw(){
	
	ofSetColor(0,0,0,255);
	font.drawString(m_node->displayName().size() > 21 && !m_grabbed? m_node->displayName().substr(0,20) + std::string("..."): m_node->displayName(), m_displayPosition.x + 15, m_displayPosition.y + 5);
	//font.drawString(getMovingStatus(), m_displayPosition.x + 15, m_displayPosition.y + 5);

	bool show_predicted_status = m_grabbed || (m_status.first != m_predictedStatus.first);
	if(show_predicted_status) {
		switch(m_predictedStatus.first) {
			case NODE_IN_DOCK: 
				if(m_status.first != NODE_IN_DOCK) // docked
					ofSetColor(0, 255, 0, 60);
				else // moved
					ofSetColor(0, 255, 99, 0);
				break;
				
			case NODE_IN_CLUSTER: // always just moved
				ofSetColor(0, 255, 99, 0);
				break;
			case NODE_IN_NOWHERE:
			case NODE_IN_FREE_AREA:
			default:
				ofSetColor(0, 0);
				break;
		}
		ofEllipse(m_displayPosition.x, m_displayPosition.y, 2*m_r, 2*m_r);
	}

	switch(m_node->getType()) {
		case QUERY:
		ofSetColor(128, 128, 128, 255);
		break;
		case PAPER:
		ofSetColor(0, 99, 255, 255);
		break;
		case KEYWORD:
		ofSetColor(128, 16, 66, 255);
		break;
		case AUTHOR:
		ofSetColor(99, 255, 99, 255);
		break;
		default:
		break; 
	}
	ofEllipse(m_displayPosition.x, m_displayPosition.y, 16, 16);
	
	if (m_displayPosition != m_position){
		switch(m_node->getType()) {
			case QUERY:
			ofSetColor(128, 128, 128, 64);
			break;
			case PAPER:
			ofSetColor(0, 99, 255, 64);
			break;
			case KEYWORD:
			ofSetColor(128, 16, 66, 64);
			break;
			case AUTHOR:
			ofSetColor(99, 255, 99, 64);
			break;
			default:
			break; 
		}
		ofEllipse(m_position.x, m_position.y, 16, 16);
	}
	
}

void UINode::move(float xSpeed, float ySpeed){
	if (m_node->getType() != QUERY){
		m_displayPosition.x += xSpeed;
		m_displayPosition.y += ySpeed;
	}
	
}

bool UINode::isWithin(float x, float y){
	std::cout << "finger x: " << x << ", finger y: " << y << ", node x: " << m_position.x << ", node y: " << m_position.y << ", distance: " << ofDist(x, y, m_position.x, m_position.y)  << std::endl;
	return (ofDist(x, y, m_position.x, m_position.y) <= s_rTouch);
}

float UINode::getDist(float x, float y){
	return ofDist(x, y, m_position.x, m_position.y);
}

void UINode::setGrabbed(bool grabbed){
	m_grabbed = grabbed;
}

bool UINode::getFromDock(){
	return m_status.first == NODE_IN_DOCK;
}

NodeStatus UINode::getStatus() const
{
	return m_status;
}

NodeStatus UINode::getPredictedStatus() const
{
	return m_predictedStatus;
}

void UINode::setPredictedStatus(const NodeStatus & status)
{
	m_predictedStatus = status;
}

void UINode::setStatus(const NodeStatus & status)
{
	m_status = status;
}

std::shared_ptr<Node> UINode::getNode()
{
	return m_node;
}

bool UINode::docked() const
{
	return m_status.first == NODE_IN_DOCK;
}

