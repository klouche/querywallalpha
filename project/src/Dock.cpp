//
//  Dock.cpp
//  emptyExample
//
//  Created by Khalil Klouche on 09/04/14.
//

#include "Dock.h"
#include "TableApp.hpp"


void Dock::setup(string query0){
	m_query = query0;

	m_handlePos = 0.8 * ofGetHeight(); //line between queries and results
	m_xOffset = 0;
	m_width = 1000;

	QueryParameters params;
	params.m_query = query0;

	std::shared_ptr<Node> queryNode(new Node);
	queryNode->setType(QUERY);
	queryNode->setDisplayName(m_query);

    std::shared_ptr<UINode> queryUINode(new UINode);

    float y = ofGetHeight() - 50;
    queryUINode->setup(queryNode, m_query, ofVec2f(120, y));

    m_queryQueue.push_back(std::make_pair(params, queryUINode));

	registerEvent(InputGestureDirectFingers::Instance().newCursor, &Dock::newCursor, this);
	registerEvent(InputGestureDirectFingers::Instance().updateCursor, &Dock::updateCursor, this);
	registerEvent(InputGestureDirectFingers::Instance().removeCursor, &Dock::removeCursor, this);

	registerEvent(nodeChanged, &Dock::nodeChangedCallback, this);
}

std::shared_ptr<UINode> Dock::getNodeAt(ofVec2f location, std::shared_ptr<Cluster> * insideCluster)
{
	for(Clusters::iterator it = m_clusters.begin(); it != m_clusters.end(); ++it) {
		Cluster & cluster = **it;
		if (cluster.isWithin(location.x)) {
			int nodeIndex = cluster.getNodeWithin(location.x, location.y);
			if (nodeIndex >= 0) {
				if(insideCluster)
					*insideCluster = *it;
				return cluster.getNode(nodeIndex);
			}
		}
	}
	return std::shared_ptr<UINode>();

}
void Dock::nodeChangedCallback(NodeStatusChangeArgs & args)
{
    std::shared_ptr<UINode> uiNode = args.node;

    NodeStatusType t1 = uiNode->getStatus().first, t2 = uiNode->getPredictedStatus().first;

    bool create_new_cluster = t1 == NODE_IN_CLUSTER && t2 == NODE_IN_DOCK;
    if(t1 == NODE_IN_DOCK && t1 == t2) {
        Cluster * cl1 = boost::get<NodeStatusInfo<NODE_IN_DOCK> >(uiNode->getStatus().second).cluster;
        Cluster * cl2 = boost::get<NodeStatusInfo<NODE_IN_DOCK> >(uiNode->getPredictedStatus().second).cluster;
        if(cl1 != cl2)
            create_new_cluster = true;
    }

    if(create_new_cluster) {
        std::shared_ptr<UINode> duplicate(new UINode(*uiNode));

        duplicate->setStatus( uiNode->getPredictedStatus() );
        duplicate->setGrabbed(false);
        duplicate->setPosition(duplicate->getDisplayPosition());

        // All but the first fixed query UINode should have a Node?
        if(!uiNode->getNode())
          return;

        std::string query = uiNode->getNode()->getStringId();
        QueryParameters params;
        params.m_query = m_query;

        Cluster * cluster = boost::get<NodeStatusInfo<NODE_IN_DOCK> >(uiNode->getPredictedStatus().second).cluster;

        // Check if the node was dropped inside of an existing cluster
        if(cluster) {
            std::shared_ptr<Result> result = cluster->getResult();
            if(result) {
                params.m_priors = result->m_params.m_priors;
                params.m_cluster = cluster;
            }
        }

        params.m_priors.insert(query);

        m_queryQueue.push_back(std::make_pair(params, duplicate));
    } else if(t1 == NODE_IN_CLUSTER && t2 == NODE_IN_CLUSTER) {
        typedef NodeStatusInfo<NODE_IN_CLUSTER> CInfo;
        CInfo s1 = boost::get<CInfo>(uiNode->getStatus().second);
        CInfo s2 = boost::get<CInfo>(uiNode->getPredictedStatus().second);
        if(s1.cluster == s2.cluster) {
            // Move inside a cluster
            uiNode->setPosition(uiNode->getDisplayPosition());
        }
    }
}

void Dock::newCursor(InputGestureDirectFingers::newCursorArgs & args)
{
	DirectFinger * finger = args.finger;

	float x = finger->getX() / (TableApp::getWidth() / TableApp::getHeight()) * ofGetWidth();
	float y = finger->getY() * ofGetHeight();
	int id = finger->s_id;

	m_cursors.insert( std::make_pair(id, ofVec2f(x,y)));

	std::shared_ptr<Cluster> cluster;
	std::shared_ptr<UINode> node = getNodeAt(ofVec2f(x,y), &cluster);
	if(node) {
		NodeStatusChangeArgs args;
		args.node = node;
		m_grabbedCursors.insert(std::make_pair(id, args));
		node->setGrabbed(true);
	}
}
    
bool Dock::isInsideDock(ofVec2f pos) const
{
	return pos.y > m_handlePos;
}

void Dock::updateCursor(InputGestureDirectFingers::updateCursorArgs & args)
{
	DirectFinger * finger = args.finger;

	float x = finger->getX() / (TableApp::getWidth() / TableApp::getHeight()) * ofGetWidth();
	float y = finger->getY() * ofGetHeight();

	float xPrev = x;
	float yPrev = y;

	int id = finger->s_id;
	//float xSpeed = finger->xspeed;
	//float ySpeed = finger->yspeed;
	float motion_accel = finger->maccel;

	CursorMap::iterator it2 = m_cursors.find(id);
	if(it2 != m_cursors.end())
	{
		xPrev = m_cursors[id].x;
		yPrev = m_cursors[id].y;
		m_cursors[id].x = x;
		m_cursors[id].y = y;
	}
	float xSpeed = x - xPrev;
	float ySpeed = y - yPrev;

	GrabMap::iterator it = m_grabbedCursors.find(id);
	if(it != m_grabbedCursors.end())	
	{
		std::shared_ptr<UINode> node = it->second.node;
		node->move(xSpeed, ySpeed);

   		std::shared_ptr<Cluster> cluster = getCluster(node->getDisplayPosition().x);

		if(isInsideDock(node->getDisplayPosition())) {
			NodeStatus status;
			NodeStatusInfo<NODE_IN_DOCK> info;			
			info.cluster = cluster.get();

			status.first = NODE_IN_DOCK;
			status.second = info;
			node->setPredictedStatus(status);
		} else if(cluster) {
            NodeStatus status;
            NodeStatusInfo<NODE_IN_CLUSTER> info;
            info.cluster = cluster.get();

            status.first = NODE_IN_CLUSTER;
            status.second = info;
            node->setPredictedStatus(status);
		} else {
            NodeStatus status;
            NodeStatusInfo<NODE_IN_FREE_AREA> info;
            status.first = NODE_IN_FREE_AREA;
            status.second = info;
            node->setPredictedStatus(status);
		}
	}


	std::cout << "Finger " << id << " @(" << x << ", " << y << ")" << ", speed (" << xSpeed << ", " << ySpeed << "), motion acc " << motion_accel << std::endl;
}

void Dock::removeCursor(InputGestureDirectFingers::removeCursorArgs & args)
{
	DirectFinger * finger = args.finger;

	int id = finger->s_id;

	GrabMap::iterator it = m_grabbedCursors.find(id);
	if(it != m_grabbedCursors.end())
	{
		NodeStatusChangeArgs & args = it->second;
		std::shared_ptr<UINode> node = args.node;
		ofNotifyEvent(nodeChanged, args, this);
		node->setGrabbed(false);
		m_grabbedCursors.erase(id);
	}

	CursorMap::iterator it2 = m_cursors.find(id);
	if(it2 != m_cursors.end())
	{
		m_cursors.erase(id);
	}
}

std::shared_ptr<Cluster> Dock::getCluster(float x)
{
    for(Clusters::iterator it = m_clusters.begin(); it != m_clusters.end(); ++it) {
		if ((**it).isWithin(x)){
            return *it;
		}
    }
    return std::shared_ptr<Cluster>();
}

void Dock::update(){

    if(!m_newResult.second && m_queryQueue.size() > 0 ) {
        std::pair<QueryParameters, std::shared_ptr<UINode > > p = m_queryQueue.front();
        m_queryQueue.erase(m_queryQueue.begin());

        Poco::ActiveResult<std::shared_ptr<Result> > resFuture = m_fetcher.getResult(p.first);
        m_newResult.first = p.second;
        m_newResult.second.reset(new Poco::ActiveResult<std::shared_ptr<Result> >(resFuture));
        std::cout << "new result " << std::endl;
    }

	if(m_newResult.second) {
		if(m_newResult.second->available()) {
			std::shared_ptr<Result> res = m_newResult.second->data();

			if(res) {
                /// FIXME: this is not really safe, should check if existing cluster actually still exists
                Cluster * existing = res->m_params.m_cluster;

                bool new_cluster = !existing;

                if(!existing) {
                  existing = new Cluster;
                }

                ofVec2f pos = m_newResult.first->getPosition();

                existing->setup(res, ofVec2f(pos.x, 0));

                //m_newResult.first->setPosition(ofVec2f(0, pos.y));

		NodeStatus status;
		NodeStatusInfo<NODE_IN_DOCK> info;
		status.first = NODE_IN_DOCK;
		status.second = info;
		
                m_newResult.first->setStatus(status);
                m_newResult.first->setPredictedStatus(status);

                existing->addNode(m_newResult.first);

                if(new_cluster)
				  m_clusters.push_back(std::shared_ptr<Cluster>(existing));
			}

            m_newResult.first.reset();
			m_newResult.second.reset();
		} else if(m_newResult.second->failed()) {
		    m_newResult.first.reset();
			m_newResult.second.reset();
		}
	}

	for(Clusters::iterator it = m_clusters.begin(); it != m_clusters.end(); ++it) {
		(*it)->update();
	}
}

void Dock::draw(){
	ofBackground(255);
//ofBackgroundGradient(ofColor(200,200,255),ofColor(220,220,220), OF_GRADIENT_LINEAR);

	// Drawing clusters
	for(Clusters::iterator it = m_clusters.begin(); it != m_clusters.end(); ++it) {
		(*it)->draw();
	}

	// Draw stuff in queue
	for(QueryQueue::iterator it = m_queryQueue.begin(); it != m_queryQueue.end(); ++it) {
		(*it).second->draw();
	}
	if(m_newResult.first && m_newResult.second)
		m_newResult.first->draw();

    ofSetColor(192);
    for(CursorMap::iterator it = m_cursors.begin(); it != m_cursors.end(); ++it) {
        ofVec2f loc = it->second;
        ofCircle(loc.x, loc.y, 15);
    }

	// Drawing horizontal line
	ofSetColor(0);
	ofSetLineWidth(2);
	ofLine(0,m_handlePos, ofGetWidth(), m_handlePos);

}
