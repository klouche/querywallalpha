
// Sadly have to include this first since openframeworks includes will mess up stuff
#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/reader.h"
#include "rapidjson/filestream.h"

#include "ResultFetcher.h"

#include "ofMain.h"

#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/HTTPResponse.h>

#include <Poco/StreamCopier.h>

#include <sstream>


namespace {

/// TODO check if this adapter if needed, currently parsing is started when the contents are fully received
#if 0
template<typename T>
class std_istream_to_rapidjson_stream<T>
{
public:
    T Peek() const
    {


    }

    T Take()
    {


    }

    size_t Tell() const
    {

    }

    T* PutBegin() { return 0; }
    void Put(T) {}
    size_t PutEnd(T*) { return 0; }
}
#endif

bool parseDocument(rapidjson::Document & doc, Result & output)
{

   if(!doc.HasMember("playground"))
     return false;

   if(!doc["playground"].HasMember("allEntities"))
     return false;

   if(!doc["playground"].HasMember("topEntities"))
     return false;

   // for now, just collect top entities
   rapidjson::Value & topEntities = doc["playground"]["topEntities"];
   if(!topEntities.IsArray())
     return false;

   std::set<std::string> topSet;
   for(rapidjson::SizeType i=0; i < topEntities.Size(); ++i) {
       topSet.insert(topEntities[i].GetString());
   }

   std::cout << "Top set includes " << topSet.size() << " entities" << std::endl;

   rapidjson::Value & allEntities = doc["playground"]["allEntities"];
   if(!allEntities.IsArray())
     return false;

   for(rapidjson::SizeType i=0; i < allEntities.Size(); ++i) {
       const std::string & idString = allEntities[i]["id"].GetString();
       if(topSet.count(idString) == 0)
         continue;

       const std::string & displayName = allEntities[i]["displayname"].GetString();
       const std::string & type = allEntities[i]["type"].GetString();
       NodeType nodeType = UNKNOWN;

       if(type == "key")
         nodeType = KEYWORD;
       else if(type == "article")
         nodeType = PAPER;
       else if(type == "person")
         nodeType = AUTHOR;

       std::shared_ptr<Node> node(new Node);
       node->setStringId(idString);
       node->setDisplayName(displayName);
       node->setType(nodeType);

       output.m_graph.addNode(node);
   }

    return true;
}

}

class ResultFetcher::D
{

public:
};


ResultFetcher::ResultFetcher()
  : m_d(new D)
  , getResult(this, &ResultFetcher::getResultImpl)
{
}

ResultFetcher::~ResultFetcher()
{
    delete m_d;
}



std::shared_ptr<Result> ResultFetcher::getResultImpl(const QueryParameters & params)
{
    std::shared_ptr<Result> res(new Result);
    res->m_params = params;

    const std::string host = "hand.hiit.fi";
    const uint16_t port = 8080;
    const std::string path = "/playground/service/playgroundsearch.json";

    /// TODO actually reuse session
    Poco::Net::HTTPClientSession session(host, port);
    //session.setKeepAlive(true);

    Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, path);
    request.setVersion("HTTP/1.1");

    Poco::Net::HTMLForm form;

    form.add("q", params.m_query);

    for(std::set<std::string>::iterator it = params.m_priors.begin(); it != params.m_priors.end(); ++it) {
      form.add("p[]", *it);
    }

    form.add("bsize", "1500");
    form.add("num", "5");
    // should be false if this is same as previous query but with priors changed
    form.add("makesearch", "true");

    for(Poco::Net::HTMLForm::ConstIterator it = form.begin(); it != form.end(); ++it) {
      std::cout << it->first << " = " << it->second << std::endl;
    }
  
    // This does not add the actual parameters for POST requests
    form.prepareSubmit(request);

    std::ostream & requestStream = session.sendRequest(request);

    // write post parameters
    form.write(requestStream);

    Poco::Net::HTTPResponse response;

    std::istream & is = session.receiveResponse(response);

    std::cout << "Got response, status = " << response.getStatus() << std::endl;

    std::stringstream inputString;
    Poco::StreamCopier::copyStream(is, inputString);
    //Poco::StreamCopier::copyStream(is, std::cout);

    rapidjson::Document document;

    //if(document.ParseStream<0>(is).HasParseError()) {
    if(document.Parse<0>(inputString.str().c_str()).HasParseError()) {
      std::cout << "Invalid JSON document received" << std::endl;
      return std::shared_ptr<Result>();
    } else {
      FILE * f = fopen("latest.json", "w+");
      rapidjson::FileStream os(f);
      // For some reason output is truncated to ~240kb
      rapidjson::PrettyWriter<rapidjson::FileStream> writer(os);
      rapidjson::Reader reader;
      rapidjson::GenericStringStream<rapidjson::UTF8<> > inputStream(inputString.str().c_str());
      //reader.Parse<0>(inputString.str().c_str(), writer);
      reader.Parse<0>(inputStream, writer);
      fclose(f);

      std::cout << "JSON parsing ok" << std::endl;
      if(parseDocument(document, *res)) {
        return res;
      } else {
        std::cerr << "Error processing JSON document" << std::endl;
        return std::shared_ptr<Result>();
      }
    }
}

