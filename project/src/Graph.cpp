#include "Graph.h"

#include <cassert>

namespace {
  uint64_t s_idCounter = 0;
}

Node::Node()
  : m_type(UNKNOWN)
  , m_id(s_idCounter++) // Not thread-safe
{

}

NodeType Node::getType() const
{
    return m_type;
}

void Node::setType(NodeType type)
{
    m_type = type;
}

uint64_t Node::getId() const
{
    return m_id;
}

const std::string & Node::getStringId() const
{
    return m_stringId;  
}

void Node::setStringId(const std::string & id)
{
    m_stringId = id;
}


const std::string & Node::displayName() const
{
    return m_displayName;
}

void Node::setDisplayName(const std::string & displayName)
{
    m_displayName = displayName;
}


Graph::Graph()
{
    //ctor
}

Graph::~Graph()
{
    //dtor
}

const std::vector<std::shared_ptr<Node> > & Graph::getNodes() const
{
    return m_nodes;
}

void Graph::addNode(const std::shared_ptr<Node> & node)
{
    m_nodeMap[node->m_id] = node;
    m_nodes.push_back(node);
}

void Graph::addLink(uint64_t id1, uint64_t id2)
{
    // Should not be already present
    assert(std::find(m_links[id1].begin(), m_links[id1].end(), id2) == m_links[id1].end());

    m_links[id1].push_back(id2);
    m_linkSet.insert(std::make_pair(id1, id2));
}

bool Graph::linked(uint64_t id1, uint64_t id2) const
{
    return m_linkSet.find(std::make_pair(id1, id2)) != m_linkSet.end();
}
