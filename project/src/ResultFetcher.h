#ifndef RESULTFETCHER_H
#define RESULTFETCHER_H

#include "Graph.h"
#include <string>
#include <set>

#include "types/ofTypes.h"

#include <Poco/ActiveMethod.h>

class Cluster;
struct QueryParameters
{
    QueryParameters()
    : m_cluster(0)
    {
    }

    Cluster * m_cluster;
    std::string m_query;
    std::set<std::string> m_priors;
};

class Result
{
public:
    QueryParameters m_params;

    Graph m_graph;
};

class ResultFetcher
{
    public:
        ResultFetcher();
        virtual ~ResultFetcher();


        Poco::ActiveMethod< std::shared_ptr<Result>, QueryParameters, ResultFetcher > getResult;

    protected:
    private:
      std::shared_ptr<Result> getResultImpl(const QueryParameters & params);

      class D;
      D * m_d;

};

#endif // RESULTFETCHER_H
