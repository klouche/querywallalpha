//
//  Cluster.cpp
//  emptyExample
//
//  Created by Khalil Klouche on 09/04/14.
//
//

#include "Cluster.h"

void Cluster::setup(const std::shared_ptr<Result> & result, ofVec2f location){
	m_location = ofVec2f(0, 0); //location;
	m_min = -5;
	m_max = 5;

	const std::vector<std::shared_ptr<Node> > & nodes = result->m_graph.getNodes();

    float ymin = 30;
    float ymax = ofGetHeight() - 200;
    float ystep = nodes.size() <= 1 ? 0 : (ymax-ymin) / (nodes.size() - 1);

    // Remove all non-docked nodes
    for(int i=0; i < m_nodes.size(); ++i) {
        if(!m_nodes[i]->docked()) {
            m_nodes.erase(m_nodes.begin() + i);
            i--;
        }
    }

	for(int i=0; i < nodes.size(); ++i) {
		ofVec2f pos = location;
		pos.x += ofRandom(-10, 10);
		pos.y += ymin + i*ystep;

		m_nodes.push_back(std::shared_ptr<UINode>(new UINode()));
		m_nodes.back()->setup(nodes[i], "", pos);
		
		NodeStatus status;
		status.first = NODE_IN_CLUSTER;
		NodeStatusInfo<NODE_IN_CLUSTER> info;
		info.cluster = this;
		status.second = info;

		m_nodes.back()->setStatus(status);
		m_nodes.back()->setPredictedStatus(status);
	}

	m_result = result;
}

void Cluster::update(){
	for(UINodes::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
		(*it)->update();
	}

}

void Cluster::draw(){

	ofPushMatrix();

	ofTranslate(m_location.x, m_location.y);

	ofEnableAlphaBlending();

	ofSetColor(255, 0, 0, 32);
	ofRectangle viewport = ofGetCurrentViewport();

	float min = std::numeric_limits<float>::infinity();
	float max = -min;
	for(int i=0; i < m_nodes.size(); ++i) {
		float posX = m_nodes[i]->getPosition().x;
		if(min > posX) min = posX;
		if(max < posX) max = posX;
	}

	if(m_nodes.empty()) {
		min = -5;
		max = 5;
	}
	const float margin = 50;
	min -= margin;
	max += margin;

	m_min = min;
	m_max = max;

	ofSetDepthTest(false);

	ofRect(min, viewport.getMinY(), max-min, viewport.height);

	for(UINodes::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it) {
		(*it)->draw();
	}


	ofDisableAlphaBlending();
	ofPopMatrix();
}

void Cluster::addNode(std::shared_ptr<UINode> node) {
    m_nodes.push_back(node);
}

bool Cluster::isWithin(float x){
	std::cout << "isWithin // x: " <<  x << "; min: " << m_location.x + m_min << "; max: " << m_location.x + m_max << std::endl;
	return x > m_location.x + m_min && x < m_location.x + m_max;
}

int Cluster::getNodeWithin(float x, float y){
	int closestNode = m_nodes.size();
	float closestNodeDistance = 10000;
	for (int i = 0; i < m_nodes.size(); i++){
		if (m_nodes[i]->isWithin(x - m_location.x, y - m_location.y) && m_nodes[i]->getDist(x,y) < closestNodeDistance){
			closestNode = i;
			closestNodeDistance = m_nodes[i]->getDist(x,y);
	}
}
	if (closestNode == m_nodes.size()) return -1;
	return closestNode;
}

std::shared_ptr<UINode> Cluster::getNode(int i){
	return m_nodes[i];
}
