//
//  Cluster.h
//  emptyExample
//
//  Created by Khalil Klouche on 09/04/14.
//
//

#ifndef __emptyExample__Cluster__
#define __emptyExample__Cluster__

#include <iostream>
#include "ofMain.h"
#include "UINode.h"

#include "ResultFetcher.h"

class Cluster {

public:
    void setup(const std::shared_ptr<Result> & result, ofVec2f location);
    void update();
    void draw();

	int getNodeWithin(float x, float y);
	std::shared_ptr<UINode> getNode(int i);
    std::shared_ptr<Result> getResult() { return m_result; }
    void addNode(std::shared_ptr<UINode> node);
	bool isWithin(float x);

private:
    ofVec2f m_location;
	float m_min;
	float m_max;

    typedef std::vector<std::shared_ptr<UINode> > UINodes;
    UINodes m_nodes;
    std::shared_ptr<Result> m_result;
};


#endif /* defined(__emptyExample__Cluster__) */
